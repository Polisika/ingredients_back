import json
import pickle
from merge_lists import IngredientsList, Ingredient

if __name__ == "__main__":
    ingredients: IngredientsList = pickle.load(open("ing_list.pkl", 'rb'))
    exclude_list = [Ingredient(**i) for i in json.load(open("exclude_ingredients.json"))]
    ingredients.exclude_list(exclude_list)
    print(ingredients)
