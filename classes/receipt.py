from product import Product
from store import StorageOfProducts


class Receipt(StorageOfProducts):
    name: str
    description: str
    products: list[Product]

    def __init__(self, name: str, description: str, products: list[Product]):
        super().__init__(products)
        self.name = name
        self.description = description
