import enum

from abc import ABC, abstractmethod
from enums import WeightUnit, VolumeUnit
from errors.custom_exceptions import NoSuchUnitException

weight_coefs = {WeightUnit.GRAM: 1,
                WeightUnit.KILOGRAM: 1000}

volume_coefs = {VolumeUnit.MILLILITER: 1,
                VolumeUnit.TEA_SPOON: 5,
                VolumeUnit.TABLESPOON: 15,
                VolumeUnit.OUNCE: 30,
                VolumeUnit.LITER: 1000,
                VolumeUnit.CUBIC_METER: 1000000}


class Unit(ABC):
    name: str
    density: float

    @abstractmethod
    def change_unit(self, new_unit: enum) -> float:
        pass

    @abstractmethod
    def get_absolute(self):
        pass


class Loose(Unit):
    absolute_weight: float

    def __init__(self, name: str, density: float, absolute_weight: float):
        self.name = name
        self.density = density
        self.absolute_weight = absolute_weight

    def change_unit(self, new_unit: enum) -> float:
        if new_unit in weight_coefs.keys():
            self.name = new_unit.value
            old_absolute_weight = self.absolute_weight
            self.absolute_weight = weight_coefs[new_unit]
            return old_absolute_weight / self.absolute_weight
        elif new_unit in volume_coefs.keys():
            self.name = new_unit.value
            old_absolute_weight = self.absolute_weight
            new_absolute_weight = self.density * volume_coefs[new_unit]
            self.absolute_weight = new_absolute_weight
            return old_absolute_weight / new_absolute_weight
        else:
            raise (NoSuchUnitException())

    def get_absolute(self) -> float:
        return self.absolute_weight


class Liquid(Unit):
    absolute_volume: float

    def __init__(self, name: str, density: float, absolute_volume: float):
        self.name = name
        self.density = density
        self.absolute_volume = absolute_volume

    def change_unit(self, new_unit: enum) -> float:
        if new_unit in volume_coefs.keys():
            self.name = new_unit.value
            old_absolute_volume = self.absolute_volume
            self.absolute_volume = volume_coefs[new_unit]
            return old_absolute_volume / self.absolute_volume
        elif new_unit in weight_coefs.keys():
            self.name = new_unit.value
            old_absolute_volume = self.absolute_volume
            new_absolute_volume = weight_coefs[new_unit] / self.density
            self.absolute_volume = new_absolute_volume
            return old_absolute_volume / new_absolute_volume
        else:
            raise (NoSuchUnitException())

    def get_absolute(self) -> float:
        return self.absolute_volume
