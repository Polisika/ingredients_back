from list_of_products import ListOfProducts
from store import Store


class User:
    name: str
    lists_of_products: list[ListOfProducts]
    store: Store

    def __init__(self, name: str, lists_of_products: list[ListOfProducts], store: Store):
        self.name = name
        self.lists_of_products = lists_of_products
        self.store = store

    def create_list(self, products: ListOfProducts):
        self.lists_of_products.append(products)
