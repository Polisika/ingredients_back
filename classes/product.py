from enums import *
from unit import Unit


class Product:
    name: str
    unit: Unit
    __count: float
    category: list[Category]

    def __init__(self, name: str, unit: Unit, count: float, category: list[Category]):
        self.name = name
        self.unit = unit
        self.__count = count
        self.category = category

    @property
    def count(self) -> float:
        return self.__count

    @count.setter
    def count(self, new_count: float) -> None:
        self.__count = new_count

    def change_unit(self, new_unit: Enum):
        coef = self.unit.change_unit(new_unit)
        self.count *= coef

    def __eq__(self, obj):
        return isinstance(obj, Product)\
               and self.name == obj.name\
               and self.unit.density == obj.unit.density\
               and self.category == obj.category
