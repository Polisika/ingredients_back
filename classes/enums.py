from enum import Enum


class Category(Enum):
    HOME = 'Home'
    MILK = 'Milk'
    FRUIT = 'Fruit'
    VEGETABLES = 'Vegetables'
    MEDICINE = 'Medicine'
    # something to add


class WeightUnit(Enum):
    KILOGRAM = 'кг'
    GRAM = 'гр'


class VolumeUnit(Enum):
    MILLILITER = 'мл'
    LITER = 'л'
    CUBIC_METER = 'м^3'
    TEA_SPOON = 'ч. л.'
    TABLESPOON = 'ст. л.'
    OUNCE = 'oz'
