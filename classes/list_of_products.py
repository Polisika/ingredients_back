from product import Product
from errors import custom_exceptions
from unit import Unit


class ElementOfList:
    product: Product
    checkbox: bool = False

    def __init__(self, product: Product, checkbox: bool = False):
        self.product = product
        self.checkbox = checkbox

    def __eq__(self, obj):
        return isinstance(obj, ElementOfList)\
               and obj.product == self.product


class ListOfProducts:
    products: list[ElementOfList]

    def __init__(self, products: list[ElementOfList]):
        self.products = products

    def add_product(self, product: Product):
        found_product = [item.product for item in self.products if item.product == product]
        if len(found_product) == 0:
            self.products.append(ElementOfList(product=product, checkbox=False))
        else:
            amount_to_add = product.count * product.unit.get_absolute() / found_product[0].unit.get_absolute()
            found_product[0].count = found_product[0].count + amount_to_add

    def remove_product(self, product_name: str, unit: Unit, count: float):
        found_product = [item for item in self.products if item.product.name == product_name]
        if len(found_product) == 0:
            raise custom_exceptions.NoSuchProductException()
        else:
            product_before = found_product[0].product
            count_to_remove = count * unit.get_absolute() / product_before.unit.get_absolute()
            if product_before.count - count_to_remove <= 0:
                self.products.remove(found_product[0])
            else:
                found_product[0].product.count = found_product[0].product.count - count_to_remove

    # Change(check/uncheck) checkbox of product state
    def check_product(self, elem: ElementOfList):
        if elem in self.products:
            index = self.products.index(elem)
            self.products[index].checkbox = False if self.products[index].checkbox else True
        else:
            raise (custom_exceptions.NoSuchProductException())

    def share(self):
        pass



