from pydantic import BaseModel
from product import Product
from errors import custom_exceptions
from abc import ABC


class StorageOfProducts(ABC):
    products: list[Product]

    def __init__(self, products: list[Product]):
        self.products = products

    def add_products(self, products: list[Product]):
        for product in products:
            found_product = [prod for prod in self.products if product == prod]
            if len(found_product) == 0:
                self.products.append(product)
            else:
                count_to_add = product.count * product.unit.get_absolute() / found_product[0].unit.get_absolute()
                found_product[0].count = found_product[0].count + count_to_add

    def remove_items(self, products_to_remove: list[Product]):
        for product in products_to_remove:
            if product in self.products:
                old_product = self.products[self.products.index(product)]
                weight_before = old_product.count * old_product.unit.get_absolute()
                weight_to_remove = product.count * product.unit.get_absolute()

                if weight_before > weight_to_remove:
                    raise(custom_exceptions.NotEnoughProductsException())
                elif weight_before == weight_to_remove:
                    self.products.remove(old_product)
                else:
                    old_product.count = old_product.count - weight_to_remove / old_product.unit.get_absolute()
            else:
                raise(custom_exceptions.NoSuchProductException())


class Store(StorageOfProducts):
    def __init__(self, products: list[Product]):
        super().__init__(products)

    def remove_by_indices(self, indices: list[int]):
        self.products = [product for i, product in enumerate(self.products) if i in indices]

    def get_by_index(self, index: int) -> Product:
        return self.products[index]
