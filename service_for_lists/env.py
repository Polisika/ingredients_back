from pydantic import BaseSettings


class Settings(BaseSettings):
    mongo_url: str = "mongodb://db/lists"

    class Config:
        env_file = ".env"


settings = Settings()
