import collections
from copy import copy
from pathlib import Path
import json

from rank_bm25 import BM25Okapi
import pickle


class Ingredient:
    def __init__(self, name, count, unit, **kwargs):
        self.name = name
        self.count = count
        self.unit = unit

    def __add__(self, other):
        if other is None:
            return self
        if self.unit != other.unit or self.name != other.name:
            raise ValueError("Units and names have to be equal")
        return Ingredient(self.name, self.count + other.amount, self.unit)


class IngredientsList:
    def __init__(self, ingredients_list=None):
        self.contains = collections.defaultdict(float)
        if ingredients_list:
            self.add_list(ingredients_list)

    def add(self, new_ing: Ingredient):
        self.contains[(new_ing.name, new_ing.unit)] += new_ing.count

    def add_list(self, ingredients_list):
        src = [i[0] for i in self.contains.keys()]
        corpus = [i[0].lower() for i in self.contains.keys()]
        bm25 = None
        if corpus:
            tokenized_corpus = [doc.split(" ") for doc in corpus]
            bm25 = BM25Okapi(tokenized_corpus)

        for ingredient in ingredients_list:
            if corpus and ingredient.name not in corpus:
                assert bm25, "BM25 not initialized"
                tokenized_query = ingredient.name.lower().split(" ")
                scores = bm25.get_scores(tokenized_query)
                idx = scores.argmax()
                if scores[idx] > 2:
                    ingredient.name = src[idx]
            self.add(ingredient)

    def exclude_list(self, ingredients_list):
        ingredients_list_negative = []
        for ing in ingredients_list:  # type: Ingredient
            new_ing = copy(ing)
            new_ing.count = -new_ing.count
            ingredients_list_negative.append(new_ing)

        self.add_list(ingredients_list_negative)
        old_dict = copy(self.contains)
        for key in old_dict.keys():
            if self.contains[key] <= 0:
                self.contains.pop(key)

    def __str__(self):
        return f"\n".join([str(item) for item in self.contains.items()])

    def as_dict(self):
        products = []
        for item in self.contains.items():
            products.append({"name": item[0][0], "unit": item[0][1], "count": item[1]})
        result = {"products": products}
        return result



if __name__ == '__main__':
    directory = Path()
    ing_list = IngredientsList()
    for file in directory.glob("ingredients*.json"):
        ing_list.add_list([Ingredient(**i) for i in json.load(file.open())])

    print(ing_list)
    with open("ing_list.pkl", 'wb') as f:
        pickle.dump(ing_list, f)
