from pydantic import BaseModel, validator


class Product(BaseModel):
    name: str
    unit: str
    count: float
    density: float = 1  # гр/мл
    category: list[str] = []
    check: bool = False


class ListOfProducts(BaseModel):
    name: str
    products: list[Product]
    is_recipe: bool = False


class User(BaseModel):
    user_id: str
    username: str


class ListInfo(BaseModel):
    list_id: str
    name: str
    number_of_products: int
    number_of_checked_products: int
    is_recipe: bool = False


class UserListsInfo(BaseModel):
    lists: list[ListInfo]


class UserRegister(BaseModel):
    username: str
    password: str
    email: str


class Ingredient(BaseModel):
    ingredient_id: int
    unit_id: int
    amount: int


class MergeRequest(BaseModel):
    lists: list[str]


class CreateFromRecipe(BaseModel):
    list_id: str
    coeff: float
    name: str

    @validator("coeff")
    def val_coeff(cls, v):
        assert 0 < v < 100
        return v
