import base64
import logging
import secrets
import sys

import uvicorn
from bson import ObjectId
from fastapi import FastAPI, Response, Depends, HTTPException, status
from fastapi.encoders import jsonable_encoder
from fastapi.responses import FileResponse
from fastapi.responses import JSONResponse
from fastapi.security import HTTPBasic, HTTPBasicCredentials
from motor import motor_asyncio
from starlette.middleware.cors import CORSMiddleware

from env import settings
from merge_lists import IngredientsList, Ingredient
from models import CreateFromRecipe
from models import ListOfProducts, Product, User, ListInfo, UserListsInfo, UserRegister, MergeRequest
from utils import hash_password

logger = logging.getLogger("logger")
console_handler = logging.StreamHandler(stream=sys.stdout)
logger.addHandler(console_handler)
logger.setLevel("INFO")

service = FastAPI()
client = motor_asyncio.AsyncIOMotorClient(settings.mongo_url)
db = client.get_database("lists")
lists = db.lists
lists_for_users_coll = db.lists_for_users_coll
users = db.users

recipes_db = client.get_database("recipesdb")
recipes_coll = recipes_db["recipes"]

security = HTTPBasic()


# чтобы впоследствии могли грепнуть
class WTFException(Exception):
    pass


service.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)


async def get_current_username(credentials: HTTPBasicCredentials = Depends(security)):
    user = await users.find_one({"username": credentials.username})
    if user is None:
        user = {}

    user_dict = {"user_id": str(user.get("_id")), "username": user.get("username")}

    current_password_bytes = hash_password(credentials.password).encode("utf8")
    if user:
        correct_password_bytes = bytes(user.get("password"), "UTF-8")
        is_correct_password = secrets.compare_digest(
            current_password_bytes, correct_password_bytes
        )
    else:
        is_correct_password = False

    if not is_correct_password:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Incorrect email or password",
            headers={"WWW-Authenticate": "Basic"},
        )
    return User(**user_dict)


@service.post("/register")
async def register_user(body: UserRegister):
    user = await users.find_one(
        {"$or": [{"username": body.username}, {"email": body.email}]}
    )
    if user:
        return HTTPException(
            status_code=status.HTTP_409_CONFLICT,
            detail="This user already exists",
            headers={"WWW-Authenticate": "Basic"},
        )
    body.password = hash_password(body.password)
    result = await users.insert_one(body.dict())
    return {
        "id": str(result.inserted_id),
        "result": "ok",
        "token": "Basic " + str(base64.b64encode(f"{body.username}:{body.password}".encode()))[2:-1],
    }


# Создание списка продуктов
@service.post("/")
async def create_list(lp: ListOfProducts, user: User = Depends(get_current_username)):
    return await create_list_method(lp, user)


# Получение списка продуктов
@service.get("/list/{list_id}")
async def get_list(list_id: str, user: User = Depends(get_current_username)):
    list_p = await lists.find_one({"_id": ObjectId(list_id)})

    if list_p["is_recipe"]:
        is_available = True
    else:
        result = await lists_for_users_coll.find_one({"_id": ObjectId(list_id)})
        is_available = ObjectId(user.user_id) in result.get("users_id")
    if is_available:
        data = {
            "list_id": str(list_p["_id"]),
            "name": list_p["name"],
            "products": list_p["products"],
        }
        code = 200
    else:
        data = "No access!"
        code = 403
    return JSONResponse(status_code=code, content=data)


# Удаление списка продуктов
@service.delete("/{list_id}")
async def delete_list(list_id: str, user: User = Depends(get_current_username)):
    await lists.delete_one({"_id": ObjectId(list_id)})
    await lists_for_users_coll.delete_one({"_id": ObjectId(list_id)})
    data = f"List of products {list_id} deleted"
    code = 200
    return JSONResponse(status_code=code, content=data)


# Поделиться списком продуктов
@service.put("/share/{list_id}/{username}")
async def share_list(
    list_id: str, username: str, owner: User = Depends(get_current_username)
):
    result = await lists_for_users_coll.find_one({"_id": ObjectId(list_id)})
    if result is None:
        return JSONResponse(
            status_code=406, content="There is no such list of products!"
        )

    if ObjectId(owner.user_id) in result.get("users_id"):
        user = await users.find_one({"username": username})
        if not user:
            return JSONResponse(status_code=406, content="There is no such user!")

        await lists_for_users_coll.update_one(
            {"_id": ObjectId(list_id)},
            {"$addToSet": {"users_id": {"$each": [user.get("_id")]}}},
        )
        return Response(status_code=204)
    else:
        return JSONResponse(status_code=403, content="No access!")


# Редактировать список продуктов (добавить/изменить продукт)
@service.put("/edit/{list_id}")
async def edit_list(
    list_id: str, product: Product, user: User = Depends(get_current_username)
):
    #
    # result = await lists_for_users_coll.find_one({"_id": ObjectId(list_id)})
    # if result is None:
    #     return JSONResponse(
    #         status_code=406, content="There is no such list of products!"
    #     )
    #
    # if ObjectId(user.user_id) in result.get("users_id"):
    result = await lists.update_one(
        {"_id": ObjectId(list_id), "products.name": product.name},
        {"$set": {"products.$": jsonable_encoder(product)}},
    )

    if result.modified_count == 0:
        await lists.update_one(
            {"_id": ObjectId(list_id)},
            {"$push": {"products": jsonable_encoder(product)}},
        )
    return Response(status_code=204)
    # else:
    #     return JSONResponse(status_code=403, content="No access!")


# Редактировать список продуктов (удалить продукт)
@service.put("/delete_product/{list_id}")
async def delete_product_from_list(
    list_id: str, product: Product, user: User = Depends(get_current_username)
):
    # result = await lists_for_users_coll.find_one({"_id": ObjectId(list_id)})
    # if result is None:
    #     return JSONResponse(
    #         status_code=406, content="There is no such list of products!"
    #     )

    # if ObjectId(user.user_id) in result.get("users_id"):
    result = await lists.update_one(
        {"_id": ObjectId(list_id)},
        {
            "$pull": {
                "products": {
                    "name": product.name,
                    "density": product.density,
                    "unit": product.unit,
                    "category": product.category,
                }
            }
        },
    )
    if result.modified_count == 0:
        data = "There is no such product!"
        return JSONResponse(status_code=406, content=data)
    return Response(status_code=204)
    # else:
    #     return JSONResponse(status_code=403, content="No access!")


@service.post("/create_from_recipe")
async def create_from_recipe(req: CreateFromRecipe, user: User = Depends(get_current_username)):
    row = await lists.find_one({"_id": ObjectId(req.list_id)})
    if row is None:
        raise HTTPException(status_code=404, detail="Recipe not found")
    new_list = ListOfProducts(**row)
    new_list.is_recipe = False
    new_list.name = req.name
    for product in new_list.products:
        product.count *= req.coeff
        product.check = False

    return await create_list_method(new_list, user)


async def create_list_method(new_list, user):
    result = await lists.insert_one(jsonable_encoder(new_list))
    list_id = result.inserted_id
    if not new_list.is_recipe:
        await lists_for_users_coll.insert_one(
            {"_id": list_id, "users_id": [ObjectId(user.user_id)]}
        )
    code = 201
    data = {"list_id": str(list_id), "name": new_list.name}
    return JSONResponse(status_code=code, content=data)


# Получение мета-информации о списке
@service.get("/info", response_model=UserListsInfo)
async def get_info(user: User = Depends(get_current_username)):
    counter, response = await get_list_lists(user)

    if counter == 0:
        return JSONResponse(status_code=406, content="No lists fo this user!")

    return UserListsInfo(**({"lists": response}))


async def get_list_lists(user):
    counter = 0
    response = []
    agg = lists_for_users_coll.find(
        {"users_id": ObjectId(user.user_id)}
    )

    async for list_for_user in agg:
        counter += 1
        result = await lists.find_one({"_id": list_for_user.get("_id")})
        products = [
            product for product in result.get("products") if product.get("check")
        ]
        list_info = {
            "list_id": str(list_for_user.get("_id")),
            "name": result.get("name"),
            "number_of_products": len(result.get("products")),
            "number_of_checked_products": len(products),
        }
        response.append(ListInfo(**list_info))
    return counter, response


@service.get("/info/recipes")
async def get_info(user: User = Depends(get_current_username)):
    result = await get_recipes_info_full()

    if len(result) == 0:
        return JSONResponse(status_code=406, content={"detail": "No recipes!"})

    return result


async def get_recipes_info_full():
    result = []
    async for i in lists.find({"is_recipe": True}):
        result.append(ListInfo(**{
            "list_id": str(i["_id"]),
            "name": i["name"],
            "number_of_products": len(i["products"]),
            "number_of_checked_products": 0,
            "is_recipe": True,
        }))
    return result


@service.get("/full_info")
async def get_full_info(user: User = Depends(get_current_username)):
    result = await get_recipes_info_full()
    counter, response = await get_list_lists(user)

    return response + result


@service.post("/merge")
async def merge(req: MergeRequest, user: User = Depends(get_current_username)):
    ids = [ObjectId(i) for i in req.lists]
    avail_lists = {str(i["_id"]) async for i in lists_for_users_coll.aggregate([
        {"$match": {"_id": {"$in": ids}}},
        {"$unwind": "$users_id"},
        {"$match": {"users_id": ObjectId(user.user_id)}},
        {"$project": {"_id": 1}}
    ])}
    receipts = {str(i["_id"]) async for i in lists.find({"is_recipe": True}, {"_id": 1})}
    all_lists = avail_lists.union(receipts)

    if set(req.lists) - all_lists:
        forbidden = set(req.lists) - all_lists
        return JSONResponse(status_code=406, content={"ids": list(forbidden)})

    lists2merge = [i async for i in lists.find({"_id": {"$in": ids}})]
    result = IngredientsList()
    for _l in lists2merge:
        res = [Ingredient(**_p) for _p in _l["products"]]
        result.add_list(res)

    r = result.as_dict()
    return r


@service.get("/get_application")
async def download():
    return FileResponse("/shared_files_nginx/app-debug.apk")


if __name__ == "__main__":
    uvicorn.run(service, host="0.0.0.0", port=8000)
