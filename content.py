a = [{
    "name": "Основа тыквенного чизкейка",
    "products": [{"name": "сливочное масло", "count": 50, "unit": "гр."},
                 {"name": "коричневый сахар", "count": 35, "unit": "гр."}, {"name": "яйца", "count": 1, "unit": "шт."},
                 {"name": "мука", "count": 80, "unit": "гр."}, {"name": "карамель", "count": 45, "unit": "гр."},
                 {"name": "сливки", "count": 10, "unit": "гр."}, {"name": "разрыхлитель", "count": 2, "unit": "гр."},
                 {"name": "яблоки", "count": 1, "unit": "шт."}, {"name": "соль", "count": 1, "unit": "щепотка"}],
    "is_recipe": True
},
    {
        "name": "Основа черничного чизкейка",
        "products": [{"name": "мука", "count": 60, "unit": "гр.", "density": 1, "category": [], "check": False},
                     {"name": "какао", "count": 15, "unit": "гр.", "density": 1, "category": [], "check": False},
                     {"name": "сахар", "count": 75, "unit": "гр.", "density": 1, "category": [], "check": False},
                     {"name": "соль", "count": 0.25, "unit": "ч. л.", "density": 1, "category": [], "check": False},
                     {"name": "сода", "count": 0.33, "unit": "ч. л.", "density": 1, "category": [], "check": False},
                     {"name": "молоко", "count": 75, "unit": "гр.", "density": 1, "category": [], "check": False},
                     {"name": "сливочное масло", "count": 15, "unit": "гр.", "density": 1, "category": [],
                      "check": False},
                     {"name": "растительное масло", "count": 15, "unit": "гр.", "density": 1, "category": [],
                      "check": False},
                     {"name": "яйца", "count": 0.5, "unit": "шт.", "density": 1, "category": [], "check": False},
                     {"name": "ванильный экстракт", "count": 0.5, "unit": "ч. л.", "density": 1, "category": [],
                      "check": False},
                     {"name": "уксус", "count": 0.5, "unit": "ч. л.", "density": 1, "category": [], "check": False}],
        "is_recipe": True
    }
]
b = [
    {
        "name": "Слой черничный чизкейк",
        "products": [{"name": "сливочный творожный сыр", "count": 550, "unit": "гр."},
                     {"name": "сахар", "count": 70, "unit": "гр."}, {"name": "яйца", "count": 2, "unit": "шт."},
                     {"name": "черный шоколад", "count": 60, "unit": "гр."},
                     {"name": "молочный шоколад", "count": 60, "unit": "гр."},
                     {"name": "белый шоколад", "count": 60, "unit": "гр."},
                     {"name": "сливки", "count": 180, "unit": "гр."}],
        "is_recipe": True
    },
    {
        "name": "Слой тыквенный чизкейк",
        "products": [{"name": "маскарпоне", "count": 250, "unit": "гр."},
                     {"name": "сливочный сыр", "count": 200, "unit": "гр."},
                     {"name": "сахар", "count": 95, "unit": "гр."},
                     {"name": "тыквенное пюре", "count": 300, "unit": "гр."},
                     {"name": "яйца", "count": 2, "unit": "шт."}, {"name": "сливки", "count": 80, "unit": "гр."}],
        "is_recipe": True
    },
    {
        "name": "Имбирное тесто",
        "products": [
            {
                "name": "Сахар",
                "unit": "грамм",
                "density": 1,
                "count": 500,
                "check": False
            },
            {
                "name": "Вода",
                "unit": "мл",
                "density": 1,
                "count": 200,
                "check": False
            },
            {
                "name": "Сливочное масло 82.5%",
                "unit": "грамм",
                "density": 1,
                "count": 200,
                "check": False
            },
            {
                "name": "Мука",
                "unit": "грамм",
                "density": 1,
                "count": 850,
                "check": False
            },
            {
                "name": "Яйцо",
                "unit": "шт.",
                "density": 1,
                "count": 1,
                "check": False
            },
            {
                "name": "Соль",
                "unit": "ч.л.",
                "density": 1,
                "count": 1,
                "check": False
            },
            {
                "name": "Сода",
                "unit": "ч.л.",
                "density": 1,
                "count": 1,
                "check": False
            },
            {
                "name": "Корица",
                "unit": "ч.л.",
                "density": 1,
                "count": 1.5,
                "check": False
            },
            {
                "name": "Мускатный орех",
                "unit": "ч.л.",
                "density": 1,
                "count": 1,
                "check": False
            },
            {
                "name": "Кардамон",
                "unit": "ч.л.",
                "density": 1,
                "count": 0.5,
                "check": False
            },
            {
                "name": "Гвоздика",
                "unit": "ч.л.",
                "density": 1,
                "count": 0.5,
                "check": False
            },
            {
                "name": "Имбирь",
                "unit": "ч.л.",
                "density": 1,
                "count": 1,
                "check": False
            },
            {
                "name": "Белый перец",
                "unit": "ч.л.",
                "density": 1,
                "count": 0.5,
                "check": False
            }
        ],
        "is_recipe": True
    }
]
