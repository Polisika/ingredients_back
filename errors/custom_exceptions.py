class NotEnoughProductsException(Exception):
    def __init__(self, message='Not enough products to remove'):
        super().__init__(message)


class NoSuchProductException(Exception):
    def __init__(self, message='There is no such product'):
        super().__init__(message)


class NoSuchUnitException(Exception):
    def __init__(self, message='There is no such unit'):
        super().__init__(message)
        
